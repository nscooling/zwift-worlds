import sys
if sys.version_info < (3, 0):
    # Python 2
    import Tkinter as tk
    from Tkinter import *
else:
    # Python 3
    import tkinter as tk
    from tkinter import *


from zwift_course_setter import *

current = getCurrentWorld()

root = tk.Tk()
root.minsize(width=200, height=150)
root.title("Zwift World Configuration")

v = IntVar()

def ShowChoice():
    choice = v.get()
    updatePrefs(choice)
    sys.exit(0)

def Exit():
	sys.exit(0)


R0 = tk.Radiobutton(root, padx = 10, text="Default",  variable=v, value=0)
R0.pack(anchor='w')
R1 = tk.Radiobutton(root, padx = 10, text="Watopia",  variable=v, value=1)
R1.pack(anchor='w')
R2 = tk.Radiobutton(root, padx = 10, text="Richmond", variable=v, value=2)
R2.pack(anchor='w')
R3 = tk.Radiobutton(root, padx = 10, text="London",   variable=v, value=3)
R3.pack(anchor='w')

buttons = [R0, R1, R2, R3]


buttons[current].invoke()

tk.Button(root, text="Update and Exit", command=ShowChoice).pack()
tk.Button(root, text="Quit", command=Exit).pack()


tk.mainloop()
