### Setting ZWIFT Worlds on OSX ###

* This is a quick and dirty python project to set Zwift worlds
* version 0.3
* AFAIK it supports both python2 and python3

### How do I get set up? ###

* This current assumes that the Zwift prefs file is located at ```~/Documents/Zwift/prefs.xml```
* to download 
	* open a terminal window 
	* ```$ cd```
	* ```$ git clone https://nscooling@bitbucket.org/nscooling/zwift-worlds.git```
	* ```$ cd zwift-worlds```
	

### Running GUI version ###

* From the finder look into the folder ```zwift-worlds/dist```, here you should find the ```ZwiftWorld App```.


* the ZwiftWorlds App can be invoked from here, or alternatively you can copy it to the ```Applications``` folder
	* NOTE: I've found that I have to invoke it twice at the moment; I'm looking into this but seems to be a problem with the py2App tool that generates the OSX App.
		
* this will display a pop-up window with the current world setting and allow you to modify it

* Alternatively the GUI version can be run from the command line
 ```$ python ZwiftWorlds.py```



### Running CLI version ###

	* ```$ python zwift_course_setter.py```
* this will display the current world setting and allow you to modify it, e.g.

> $ python zwift_course_setter.py  
> current world is Default  
> Select the world you'd like to ride in:  
0. Default  
1. Watopia  
2. Richmond  
3. London  
4. Leave as is  
\> 1  
+++ Watopia World set +++  

* **NOTE:** It does not do any input sanitisation, so will crash if you don't give it a number

	
### Plans ###

* ~~I intend to add a simple GUI interface~~
* ~~Pack up as OSX App~~
* Move graphics to use wxPython rather than Tkinter
* At the moment I haven't planned on adding further field configurations (as per *Jesper Rosenlund Nielsen* tool) but if there are major ones then let me know and I'll look into it

### Who do I talk to? ###

* please raise any issues on this site