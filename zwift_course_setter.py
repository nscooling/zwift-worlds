import os
from os.path import expanduser

import xml.etree.ElementTree as ET

worlds = ['Default','Watopia', 'Richmond', 'London']


def getCurrentWorld():
	tree, xmlRoot, datefile = getTreeAndRoot()
	element = xmlRoot.find('WORLD')
	if element is None:
		return 0
	else:
		return int(element.text)


def getNewSettings(current):
	print("current world is " + worlds[current])
	print("Select the world you'd like to ride in:")
	print('0. Default')
	print('1. Watopia')
	print('2. Richmond')
	print('3. London')
	print('4. Leave as is')
	value = input('> ')
	return value

def updatePrefs(value):
	tree, xmlRoot, datefile = getTreeAndRoot()
	world_setup = True
	element = xmlRoot.find('WORLD')

	if element is None:
		world_setup = False

	if value == 0:
		if world_setup == True:
			xmlRoot.remove(element)
			tree.write(datefile)
	else:
		if value > 0 and value < 4:
			if world_setup == True:
				element.text = str(value)
			else:
				appt = ET.Element("WORLD")
				appt.text = str(value)
				xmlRoot.append(appt)
		tree.write(datefile)


def getTreeAndRoot():
	datefile = expanduser('~/Documents/Zwift/prefs.xml')
	tree = ET.parse(datefile)
	root = tree.getroot()
	return tree, root, datefile

def main():
	current = getCurrentWorld()
	choice = getNewSettings(current)
	if choice >= 0 and choice <4:
		updatePrefs(choice)
		print('+++ ' + worlds[choice] + ' World set +++')


if __name__ == '__main__':
	main()